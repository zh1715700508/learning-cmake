#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include "Linklist.h"
#include "base64.h"

#define SIZE_OF_INT 64

void uart_printf(char* fmt,...)  
{  
	uint16_t i,j;
	char p[100]={0};
	va_list ap;
	va_start(ap,fmt);
	vsprintf((char*)p,fmt,ap);
	va_end(ap);
	i=strlen((const char*)p);//此次发送数据的长度
	
	for(j=0;j<i;j++)//循环发送数据
	{
		printf("%c",p[j]); 
	}
} 

void clear()//靠靠
{
    char buf[1024]={0};
    printf("Press any Key to continue...");
    fgets(buf,1024,stdin);
}


int main(void) {
	const unsigned char str[] = "a45drbcd";
    const unsigned char *sourcedata = str ;
    char base64[128];
    base64_encode(sourcedata, base64);

    printf("encode:%s\n",base64);

    char dedata[128];

    base64_decode(base64, (unsigned char*)dedata);

    printf("decode:%s\n", dedata);

    clear();    
    // return 0;

	uint32_t a = 100;
#if SIZE_OF_INT  == 32
	uint32_t *p = &a;
	a = (uint32_t)p/0x400;
	printf("%d\n",(uint32_t)p);
#else
	unsigned int *p =&a;
	a = (uint64_t)p/0x400;
	printf("%d\n",(uint64_t)p);
#endif
	uart_printf((char *)"a%d%daa\n",5,6);
	LinkList link,*node;
	Link_Create(&link);
	Link_Add(&link,1);
	Link_Add(&link,2);
	Link_Add(&link,3);
	Link_Add(&link,4);
	Link_Add(&link,5);

	Link_Insert(&link,6,7);
	Link_Delete(&link,5);
	Link_Delete(&link,5);
	node = Get_LinkNode(&link,3);
	printf("0X%p\n",node);
	clear();
	return 0;
}
