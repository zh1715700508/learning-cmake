#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include "Linklist.h"
#include "base64.h"
#include "filter.h"

void uart_printf(char* fmt,...)  
{  
	uint16_t i,j;
	char p[100]={0};
	va_list ap;
	va_start(ap,fmt);
	vsprintf((char*)p,fmt,ap);
	va_end(ap);
	i=strlen((const char*)p);//此次发送数据的长度
	
	for(j=0;j<i;j++)//循环发送数据
	{
		printf("%c",p[j]); 
	}
} 



int main(void) {
	const unsigned char str[] = "a45drbcd";
    const unsigned char *sourcedata = str ;
    char base64[128];
    base64_encode(sourcedata, base64);

    printf("encode:%s\n",base64);

    char dedata[128];

    base64_decode(base64, (unsigned char*)dedata);

    printf("decode:%s\n", dedata);

	system("pause\r\n");
    
    // return 0;

	int a = 100;
	int *p = &a;
	a = (uint32_t)p/0x400;
	printf("%d\n",(uint32_t)p);

	uart_printf((char *)"a%d%daa\n",5,6);
	LinkList link,*node;
	Link_Create(&link);
	Link_Add(&link,1);
	Link_Add(&link,2);
	Link_Add(&link,3);
	Link_Add(&link,4);
	Link_Add(&link,5);

	Link_Insert(&link,6,7);
	Link_Delete(&link,5);
	Link_Delete(&link,5);
	node = Get_LinkNode(&link,3);
	printf("0X%p\n",node);
	system("pause\r\n");


	// filter test
	float buffer[20]={0},output;
	float input[30] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
	for(int i = 0;i <30 ;i++){
		output = Movng_Average_Filter_N(input[i],buffer,20);
		printf("%f\n",output);
	}
	// return 0;
}
