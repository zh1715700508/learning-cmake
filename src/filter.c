#include "filter.h"



float Filter_Slide_Mean_Value(float Data, uint8_t limitDepth)
{
	static uint8_t count=0;//计数变量
	float sum=0;
	float value_buff[limitDepth];
	
	value_buff[count]=Data;
	
	for(uint8_t i=0;i<limitDepth;i++)
	{
	  sum+=value_buff[i];
	}
	
	count++;
	
	//当数据大于数组长度,替换数据组的一个数据,低位仍掉,先进先出
	if(count==limitDepth) 
		count=0;
	
  return ((float)sum/(float)limitDepth);
}


/*******************************************
*  @brief N阶滑动平均滤波器
*  @param float input 滤波器输入
* 		  float *buffer 存储器，用于存储中间变量，使用前应该初始化为零
*		  uint8_t N 滤波器阶数，应该和buffer大小一致
* 
*******************************************/
float Movng_Average_Filter_N(float input,float *buffer,uint8_t N){
	float output = 0;
	static uint8_t i = 0,count = 0;
	buffer[count] = input;
	count++;
//	buffer[N-1] = input;
	for(i = 0 ; i < N ;i ++){
		output += buffer[i]/N;
	}
	if(count >= N){
		count = 0;
	}
	return output;
}
/* 不同写法，但是算法有优劣之分，相比上面的，这个的算法执行时间会更长 */
float Movng_Average_Filter_N1(float input,float *buffer,uint8_t N){
	float output = 0;
	static uint8_t i = 0,count = 0;
	// output = 0;
	for(i = 0 ; i < N-1 ; i++){
			buffer[i] = buffer[i+1];
	}
	buffer[N-1] = input;
	for(i = 0 ; i < N ;i ++){
		output += buffer[i]/N;
	}
	return output;
}
