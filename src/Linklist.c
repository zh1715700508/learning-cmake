#include "Linklist.h"


/// @brief 传入一个指针，指向头节点
Status Link_Create(LinkList *link){
	// link = (LinkList*)malloc(sizeof(LinkList));
	// if(link == NULL){
	// 	printf("Failed to malloc\r\n");
	// 	return ERROR;
	// }
	link->num = 0;
	link->next = NULL;   // 建立一个带头结点的单链表
	return OK;
}

/// @brief 传入一个指针，为这个指针对应的链表头后增加一个节点
Status Link_Add(LinkList *link,DataType data){
	LinkList *p = (LinkList*)malloc(sizeof(LinkList));
	if(link == NULL){
		printf("%s:The Node is invaild in %d!\r\n",__FILE__,__LINE__);
		return ERROR;
	}
	if(p == NULL){
		printf("Failed to malloc\r\n");
		return ERROR;
	}
	link->num++;
	p->data = data;
	p->next = link->next;
	link->next = p;	
	return OK;
}

/// @brief 传入一个链表指针，删除第num个节点
Status Link_Delete(LinkList *link,uint8_t num){
//	DataType e;
	LinkList *p,*q;
	if(num > link->num){
		printf("Delete Node ERROR!\r\n");
		return ERROR;
	}
	p = link;
	for(int i = 0; i < num - 1 && (p->next) ;i++){
		p = p->next;
	}
	// p->next: 这个节点就是要删除的节点
	q = p->next;
	link->num--;
//	e = p->data;
	p->next = q->next;
	free(q);
	return OK;
}


Status Link_Insert(LinkList *link,uint8_t num ,DataType e){

	LinkList *p,*q = (LinkList*)malloc(sizeof(LinkList));
	if(q == NULL){
		printf("Failed to malloc\r\n");
		return ERROR;
	}
	if(num > link->num + 1){
		printf("Insert Node ERROR!\r\n");
		return ERROR;
	}
	p = link;
	for(int i = 0; i < num-1 && (p->next) ;i++){
		p = p->next;
	}
	link->num++;
	q->next = p->next;
	p->next = q;
	q->data = e;

	return OK;
}


Status Get_LinkData(LinkList *link,uint8_t num,DataType *e){
	LinkList *p = link;
	if(num > link->num){
		printf("Delete Node ERROR!\r\n");
		return ERROR;
	}
	for(int i = 0; i < num && (p->next) ;i++){
		p = p->next;
	}
	if(!p->next){
		printf("Delete Node ERROR!\r\n");
		return ERROR;
	}
	*e = p->data;
	return OK;
}

LinkList* Get_LinkNode(LinkList *link,uint8_t num){
	LinkList *p = link;
	if(num > link->num){
		printf("Get Node ERROR!\r\n");
		return NULL;
	}
	for(int i = 0; i < num && (p->next) ;i++){
		p = p->next;
	}
	if(!p->next){
		printf("Delete Node ERROR!\r\n");
		return NULL;
	}
	return p;
}


