#ifndef _FILTER_H_
#define _FILTER_H_

#ifdef __cplusplus
extern "C"{
#endif
#include <stdint.h>

float Movng_Average_Filter_N(float input,float *buffer,uint8_t N);
float Movng_Average_Filter_N1(float input,float *buffer,uint8_t N);
#ifdef __cplusplus
}
#endif
#endif
