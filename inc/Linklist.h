#ifndef _LINKLIST_H_
#define _LINKLIST_H_

#ifdef __cplusplus
	extern "C"{
	#endif

// #include "sys.h"
// #include "Middleware_Motor.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#define ERROR	0
#define OK		1
typedef uint8_t Status;
typedef int16_t DataType;

typedef struct LinkList
{
	uint16_t num;
	DataType data;
	struct LinkList *next;
}LinkList;

Status Link_Create(LinkList *link);
Status Link_Add(LinkList *link,DataType data);
Status Link_Delete(LinkList *link,uint8_t num);
Status Link_Insert(LinkList *link,uint8_t num ,DataType e);
Status Get_LinkData(LinkList *link,uint8_t num,DataType *e);
LinkList* Get_LinkNode(LinkList *link,uint8_t num);
#ifdef __cplusplus
	}
	#endif
#endif
