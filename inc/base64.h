#ifndef _BASE64_H_
#define _BASE64_H_

#ifdef __cplusplus
	extern "C"{
	#endif
int base64_encode(const unsigned char * sourcedata, char * base64);
int num_strchr(const char *str, char c);
int base64_decode(char * base64, unsigned char * dedata);

#ifdef __cplusplus
	}
	#endif
#endif
